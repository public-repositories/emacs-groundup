;; minimal boostrap
;; ----------------

;; source directory
(defvar +v/dir-src
  (expand-file-name (file-name-as-directory "src") user-emacs-directory)
  "Source directory.")

;; scaffolding directory
(defvar +v/dir-scaffold
  (expand-file-name (file-name-as-directory "scaffolding") +v/dir-src)
  "Scaffolding directory.")
(add-to-list 'load-path +v/dir-scaffold)

;; scaffold loading function
(defun +f/load-scaffold (scaffold)
  "Load SCAFFOLD (a file-stem of a .el file in `+v/dir-scaffold')."
  (interactive)

  (let ((feature (intern (concat "eg-" scaffold))))
    (require feature (concat scaffold ".el"))))



;; scaffolding
;; -----------

;; variables
(+f/load-scaffold "early-vars")

;; straight.el
(+f/load-scaffold "bootstrap-straight")

;; early-installs
(+f/load-scaffold "early-installs")

;; load utils
(+f/load-scaffold "early-defuns")

;; userspace
(+f/load-scaffold "userspace")



;; modules
;; -------

;; user settings
(+f/load-module "user-settings")

;; keybindings
(+f/load-module "keybindings")

;; package management
(+f/load-module "package-management")

;; shell and environment related functions
(+f/load-module "shell")

;; windows and buffers
(+f/load-module "windows-buffers")

;; dired
(+f/load-module "dired")

;; uix
(+f/load-module "uix/fonts")
(+f/load-module "uix/dashboard")
(+f/load-module "uix/modeline")
(+f/load-module "uix/utils")
(+f/load-module "uix/search")
(+f/load-module "uix/editor")
(+f/load-module "uix/filesystem-hygiene")
(+f/load-module "uix/bookmarks-registers")
(+f/load-module "uix/theme")

;; minibuffer completions
(+f/load-module "minibuffer")

;; project management
(+f/load-module "project-management")

;; org
(+f/load-module "org")

;; programming
(+f/load-module "programming/ide")
(+f/load-module "programming/julia")
(+f/load-module "programming/python")
(+f/load-module "programming/matlab")
(+f/load-module "programming/yaml")

;; writing
(+f/load-module "writing")
