;;; eg-early-vars --- early variables
;; author : Tushar Chauhan <tchauhan@mit.edu>

;;; commentary:
;; Loads variables which are needed very early in the boot.

;;; code:

;; source directory
(defvar +v/dir-src)

;; scaffolding directory
(defvar +v/dir-scaffold)

;; modules directory
(defvar +v/dir-modules
  (expand-file-name (file-name-as-directory "modules") +v/dir-src)
  "Modules directory.")

;; assets directory
(defvar +v/dir-assets
  (expand-file-name (file-name-as-directory "assets") +v/dir-src)
  "Assets directory.")

;; templates directory
(defvar +v/dir-templates
  (expand-file-name (file-name-as-directory "templates") +v/dir-src)
  "Templates directory.")

;; tmp directory
(defvar +v/dir-tmp
  (expand-file-name
   (file-name-as-directory "emacs-groundup")
   (temporary-file-directory))
  "`tmp' directory.")

(unless (file-exists-p +v/dir-tmp)
  (make-directory +v/dir-tmp))


(provide 'eg-early-vars)
;;; early-vars.el ends here
