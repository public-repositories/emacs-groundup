;;; eg-early-installs --- early installs
;; author : Tushar Chauhan <tchauhan@mit.edu>

;;; commentary:
;; Installs features which need critical updates.

;;; code:

;; packages to install before org can be used
(defvar +p/early-install '(org-roam use-package)
  "List of packages to be installed early.")

;; early-installs
(dolist (pkg +p/early-install)
  (unless (require 'pkg nil 'noerror)
    (straight-use-package pkg)))


(provide 'eg-early-installs)
;;; early-installs.el ends here
