;;; eg-userspace --- userspace variables
;; author : Tushar Chauhan <tchauhan@mit.edu>

;;; commentary:
;; Variables in the userspace.  Some of them are exposed in the default config.

;;; code:
(require 'eg-early-defuns)

;; module: keybindings
(defvar +v/leader "SPC"
  "Leader key for global shortcuts.")

(defvar +v/modeleader "SPC"
  "Modeleader key for mode-local shortcuts.")


;; module: dired
(defvar +v/path-ls-darwin "/opt/homebrew/bin/gls"
  "Path to ls or equivalent on the darwin architecture.")


;; module: uix-appearance
(defvar +v/vertical-split-threshold 125
  "Threshold for creating a vertical split.")

(defvar +v/fill-column-width 80
  "Character width to use for `fill-column`.")

; fonts
(defvar +v/font-name "JuliaMono"
  "Font name.  Defaults to JuliaMono.")
(defvar +v/font-size "12"
  "Font size.  Defaults to 12.")
(defvar +v/font-weight "regular"
  "Font weight.  Defaults to regular.")
(defvar +v/nerd-font-name "Symbols Nerd Font Mono"
  "Font to use for `nerd-icons'.  Default is `Symbols Nerd Font Mono'.")

(defvar +v/welcome-message "Welcome to Emacs Groundup v0.2 !"
  "Welcome message.")

; dashboard
(defvar +v/dashboard-n-projects 4
  "Number of projects displayed on the dashboard.")
(defvar +v/dashboard-n-files 4
  "Number of recent items displayed on the dashboard.")

(defvar +v/hooks-without-line-numbers
  '(dired-mode-hook
    vterm-mode-hook
    org-agenda-mode-hook
    imenu-list-major-mode-hook
    pdf-view-mode-hook
    pdf-outline-buffer-mode-hook)
  "Modes without line numbers.")


;; module: uix-behaviour
; user information
(defvar +v/user-full-name "DEFAULT NAME"
  "Full user name.")
(defvar +v/user-email "default.name@email.com"
  "Full email address.")

; no littering
(defvar +v/dir-no-littering ".no-littering/"
  "Default no-littering directory in `user-emacs-directory`.")
(defvar +v/reldir-no-littering-etc "etc/"
  "Default no-littering etc/ directory.")
(defvar +v/reldir-no-littering-var "var/"
  "Default no-littering var/ directory.")
(defvar +v/confirm-before-quit nil
  "Confirm before quitting.")


;; module: org
(defvar +v/dir-org "~/org/"
  "Directory scanned by `org`.")

(defvar +v/dir-org-journal "~/org/journal/"
  "Directory used by `org-journal`.")

; org-capture
(defvar +v/file-org-capture "_-capture.org"
  "Filename for `org-capture`.  The file is created as an open loop.")
(defvar +v/templates-org-capture
  '(;; Quick tasks
    ("t" . ("quick task"
	    "Tasks"
	    "** TODO [#%^{Priority|A|B|C}] %^{Task}\nSCHEDULED: %^t\n%?"))

    ;; email follow-ups
    ("e" . ("email follow-up"
	    "email follow-ups"
	    "** TODO [#%^{Priority|A|B|C}] :email: %^{Recipients}\nSCHEDULED: %^t\n%?"))

    ;; Notes
    ("n" . ("quick note"
	    "Notes"
	    "** %^{title} :triage:\n%?")))

  "Templates for `org-capture`.")

; org-roam
(defvar +v/dir-org-roam "~/org/roam/"
  "Directory scanned by `org-roam`.")
(defvar +v/filename-org-roam-db ".org-roam-db.db"
  "`org-roam' database filename.  It is stored in `+v/dir-org-roam'.")

; loops
(defvar +v/reldir-notes "notes/"
  "Notes directory.")
(defvar +v/reldir-loops-open "loops/open/"
  "Open loops directory.")
(defvar +v/reldir-loops-closed "loops/closed/"
  "Closed loops directory.")
(defvar +v/reldir-loops-onhold "loops/onhold/"
  "On-hold loops directory.")
(defvar +v/reldir-loops-discarded "loops/discarded/"
  "Discarded loops directory.")

(defvar
  +v/dirs-org-agenda
  (list (+f/concat-to-folder +v/dir-org-journal (format-time-string "%Y/"))
	(+f/concat-to-folder +v/dir-org-roam +v/reldir-notes)
	(+f/concat-to-folder +v/dir-org-roam +v/reldir-loops-open)
	(+f/concat-to-folder +v/dir-org-roam +v/reldir-loops-onhold))

  "List of folders to include in `org-agenda`.")


; bookmarks: directories
(defvar +v/keybound-dirs
  `(("d"  "home" (file-name-as-directory (getenv "HOME")))
    ("e"  "emacs" ,user-emacs-directory)
    ("l"  "openloops" ,(+f/concat-to-folder +v/dir-org-roam
                                            +v/reldir-loops-open))))

; bookmarks: files
(defvar +v/keybound-files
  `(("i" "init.el" ,(expand-file-name "init.el" user-emacs-directory))
    ("c" "capture" ,(expand-file-name "_capture.org"
                                      (+f/concat-to-folder
                                       +v/dir-org-roam
                                       +v/reldir-loops-open)))))


;; module: programming
(defvar +v/num-threads-julia "8"
  "Number of threads to run julia on.")


;; module: writing
(defvar +v/langs-jinx "en_US"
  "Languages supported by jinx.  Separated by SPC.")


(provide 'eg-userspace)
;;; userspace.el ends here
