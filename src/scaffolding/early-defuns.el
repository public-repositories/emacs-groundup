;;; eg-early-defuns --- early function definitions for emacs groundup
;; author : Tushar Chauhan <tchauhan@mit.edu>

;;; commentary:
;; A set of utility functions written for Emacs groundup.

;;; code:

;; variables
;; ---------
(defvar +v/dir-modules)
(defvar +v/dir-tmp)


;; functions
;; ---------
;; - +f/concat-to-folder
;; - +f/read-as-string
;; - +h/disable-visual-line-wrapping-hook
;; - +f/fun-ow
;; - +m/ow
;; - +f/cleanup-module
;; - +f/load-module
;; - +f/cleanup-external-module
;; - +f/load-external-module



;; Concatenate strings and output a folder
;; ---------------------------------------
;; NOTE 1 : file-name-concat is only available in Emacs > 28.1.
;;          `org' makes it available as an alias `org-file-name-compat'.
;; NOTE 2 : Yes, you may roll your own and avoid this require. Do it.
;; NOTE 3 : When Debian stable moves to Emacs 28, this require will be excluded.
(defun +f/concat-to-folder (base-dir &rest rel-dirs)

  "Generate a folder name by sequentially concatenating BASE-DIR and REL-DIRS."

  (interactive)
  (require 'org-compat)

  ;; (file-name-as-directory (apply 'file-name-concat base-dir rel-dirs)))
  (file-name-as-directory (apply 'org-file-name-concat base-dir rel-dirs)))



;; Read the contents of a file as strings
;; --------------------------------------
(defun +f/read-as-string (file-name dir-templates)

  "Read as a string, the contents of FILE-NAME from DIR-TEMPLATES.

A use case: fetch org-roam template in FILE-NAME from DIR-TEMPLATES."

  (let ((path-file (expand-file-name file-name dir-templates)))
    (with-temp-buffer (insert-file-contents path-file) (buffer-string))))



;; hook for disabling visual line mode
;; -----------------------------------
(defun +h/disable-visual-line-wrapping-hook ()

  "Disable visual line wrapping in the current buffer."

  (interactive)

  (visual-line-mode nil)
  (setq truncate-lines 1))



;; run a function in the other window
;; ----------------------------------
(defun +f/fun-ow (fun)

  "Run function FUN in other window."

  (interactive)

  (save-window-excursion (call-interactively fun))
  (switch-to-buffer-other-window (other-buffer)))



;; Macro to generate a lambda that runs in the other window
;; --------------------------------------------------------
(defmacro +m/ow (fun)

  "Generate a lambda function to FUN in the other window."

  `(lambda () (interactive) (+f/fun-ow ',fun)))



;; delete a tangled module
;; -----------------------
(defun +f/cleanup-module (module)

  "Cleanup MODULE in `+v/dir-tmp'/modules."

  ;; (interactive)

  (let* ((relpath-module (concat module ".org"))
	 (dir-tmp-modules (+f/concat-to-folder
			   +v/dir-tmp (file-name-as-directory "modules")))

	 (path-tmp-org (expand-file-name relpath-module dir-tmp-modules))
	 (path-tmp-el (concat (file-name-sans-extension path-tmp-org) ".el")))

    ;; remove the files
    (delete-file path-tmp-org)
    (delete-file path-tmp-el)))



;; load a literate config file
;; ---------------------------
(defun +f/load-module (module)

  "Load MODULE (an org-literate file-stem in `+v/dir-src')."

  (interactive)

  (let* ((relpath-module (concat module ".org"))
	 (path-module (expand-file-name relpath-module +v/dir-modules))

	 (dir-tmp-modules (+f/concat-to-folder
			   +v/dir-tmp (file-name-as-directory "modules")))

	 (path-tmp-module (expand-file-name relpath-module dir-tmp-modules))
	 (dir-tmp-module (file-name-directory path-tmp-module))

	 (fn-cleanup (apply-partially '+f/cleanup-module module)))


    ;; Make a tmp directory if it doesn't exist
    (unless (file-exists-p dir-tmp-module)
      (make-directory dir-tmp-module t))

    ;; Copy the file
    (copy-file path-module dir-tmp-module t)
    
    ;; Run the file in the tmp directory
    (org-babel-load-file path-tmp-module)

    ;; add hook to delete files
    (add-hook 'kill-emacs-hook fn-cleanup)))



;; delete a tangled external-module
;; --------------------------------
(defun +f/cleanup-external-module (module)

  "Cleanup MODULE in DIR-BASE."

  ;; (interactive)

  (let* ((relpath-module (concat module ".org"))
	 (dir-tmp-modules (+f/concat-to-folder
			   +v/dir-tmp (file-name-as-directory "modules-external")))

	 (path-tmp-org (expand-file-name relpath-module dir-tmp-modules))
	 (path-tmp-el (concat (file-name-sans-extension path-tmp-org) ".el")))

    ;; remove the files
    (delete-file path-tmp-org)
    (delete-file path-tmp-el)))


;; load an external module (literate config file in an arbitrary directory)
;; ------------------------------------------------------------------------
(defun +f/load-external-module (module dir)

  "Load external MODULE (an org-literate file-stem) from DIR."

  (interactive)

  (let* ((relpath-module (concat module ".org"))
	 (path-module (expand-file-name relpath-module dir))

	 (dir-tmp-modules (+f/concat-to-folder
			   +v/dir-tmp
			   (file-name-as-directory "modules-external")))

	 (path-tmp-module (expand-file-name relpath-module dir-tmp-modules))
	 (dir-tmp-module (file-name-directory path-tmp-module))

	 (fn-cleanup (apply-partially '+f/cleanup-external-module module)))


    ;; Make a tmp directory if it doesn't exist
    (unless (file-exists-p dir-tmp-module)
      (make-directory dir-tmp-module t))

    ;; Copy the file
    (copy-file path-module dir-tmp-module t)
    
    ;; Run the file in the tmp directory
    (org-babel-load-file path-tmp-module)

    ;; add hook to delete files
    (add-hook 'kill-emacs-hook fn-cleanup)))



(provide 'eg-early-defuns)
;;; early-defuns.el ends here
