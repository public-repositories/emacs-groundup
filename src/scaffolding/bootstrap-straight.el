;;; eg-boostrap-straight --- bootstrap straight.el
;; author : Tushar Chauhan <tchauhan@mit.edu>

;;; commentary:
;; Loads `straight.el' and installs features which need critical updates.

;;; code:


;; bootstrap `straight.el`
;; -----------------------
;; REF: https://github.com/radian-software/straight.el#getting-started
(defvar bootstrap-version)

(let* ((reldir-bootstrap "straight/repos/straight.el/bootstrap.el")
       (bootstrap-file (expand-file-name reldir-bootstrap user-emacs-directory))
       (bootstrap-version 6)
       (url-straight "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"))

  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
	(url-retrieve-synchronously url-straight 'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

;; straight profiles
(setq straight-profiles '((macos . "macos.el") (linux . "linux.el")))
(cond ((equal system-type 'darwin) (setq straight-current-profile 'macos))
      ((equal system-type 'gnu/linux) (setq straight-current-profile 'linux)))


(provide 'eg-bootstrap-straight)
;;; bootstrap-straight.el ends here
