#+html: <div align="center"><h5> <a href="./src/docs/emacs-groundup.org">home</a> | <a href="./src/docs/installation.org">installation</a> | <a href="./src/docs/notes.org">notes</a> | <a href="./src/docs/loop-management.org">loop management</a> | <a href="./src/docs/contribute.org">contribute</a> </h5></div>

* Additional Notes

Here are some additional notes. They should come in handy for those looking to customise *emacs-groundup* further. Note that, like the rest of these docs, this page also follows the conventions specified in [[./installation.org/][Installation]] for *X/* (emacs directory) and *C/* (user settings directory).


* C/settings.org

Starting off, configuring *C/settings.org* should be quite sufficient. This file contains the most relevant user-facing settings, and a default template for the file is available in *X/src/templates/*. If a settings file does not exist, it is, in fact, this default configuration that is loaded by *emacs-groundup*.

Finally, a more comprehensive list of user-facing variables can be derived from *X/scaffolding/userspace.el*.


* X/

One of the main draws of *emacs-groundup* is that the user gets to understand how their emacs is configured. To do this well you will, sooner rather than later, want to start tinkering with the configuration in *X/*. *emacs-groundup* adopts a module based structure, where each module contains a themed subset of the overall configuration.


** modules

*emacs-groundup* configuration is organised in modules, which can be found in *X/src/modules/* and its sub-directories. You could first try to tweak existing modules, and as you build more confidence, start writing your own. An important objective of the default modules is configuration *decoupling* - i.e., changing one module does not effect the others. Writing decoupled code is an excellent programming habit, and it is highly encouraged when writing *emacs-groundup* modules as well. Finally, remember to load your module by using the function ~+f/load-module~ in *X/init.el*.


** scaffolding

As you get more advanced (you don't have to), you will probably start writing your own helper functions, declaring your own configuration-related variables, and doing all kinds of voodoo to make writing and maintaining your configuration easier. At this point, it is best to begin looking more closely at *X/src/scaffolding/*. This folder contains the scaffolding required to construct *emacs-groundup* - i.e., code which must be loaded before any modules are configured. It is the ideal place for bootstrapping and maintaining a userspace.

