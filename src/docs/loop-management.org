#+html: <div align="center"><h5> <a href="./src/docs/emacs-groundup.org">home</a> | <a href="./src/docs/installation.org">installation</a> | <a href="./src/docs/notes.org">notes</a> | <a href="./src/docs/loop-management.org">loop management</a> | <a href="./src/docs/contribute.org">contribute</a> </h5></div>

* Loop Management

The note-taking and project-management structure implemented in *emacs-groundup* is based on a technique I call Loop Management. It is derived from popular and scientific reading (I work in the neurosciences and cognitive psychology), coupled with my own experience. Here is a summary in the context of *emacs-groundup*.

Loop Management is all about execution, review, and pruning. In *emacs-groundup*, it can be used by adopting a workflow which integrates ~SPC c~, ~SPC l l~, ~SPC o a a~, ~SPC o a t~, and other commands you will slowly, but surely discover. Here are a few practical keybindings and tips.

* Capturing ideas and tasks

Capturing is associated with the keybinding ~SPC c~. It is fundamental to keeping your mind focussed on the task at hand while 'capturing' stray one-offs and ideas that keep coming up.

- ~SPC c {t,e,n}~: Jot down a quick task (t), an email follow-up (e) or a short note (n). The tasks will appear in your agenda (see below).

- ~SPC o f c~: This opens the capture file and allows you to review the entire list of captures. You will want to keep the capture list as close to empty as possible. This is accomplished by organising and acting on new information as soon as you find some 'free' time.

* Loops

Loop-related keybindings are associated with ~SPC l~.

- ~SPC l l~: Start a new loop or note.

- ~SPC l {x,d,f}~: Close (x), delete/discard (d), or freeze (f) a loop.

  You close a loop when you're done. You discard it when you decide to no longer keep track of it. And you freeze it if it needs to be put on hold for some reason. It is a good idea to review your frozen loops every once in a while and delete/discard the stale ones.

* Tasks and Notes

Inside each loop, you have notes and tasks. Both are represented by a heading, followed by some text. In *org-mode*, headings start with one or more asterisks (*s).

Notes consist of a heading, followed by text. They are used to collect ideas and information about a loop. Tasks, on the other hand, are used to record plans and actionable items. While they also consist of a heading followed by text, the heading in this case has additional syntax which gives the block interesting properties - such as having a priority, a due-date, and the ability to appear in the Agenda (see below).

The following keybindings are used to deal with tasks: 

- ~SPC SPC t {t,w,d,c,j,k,x}~: Assign a TODO (T), WAITING (W), DONE (D), or CANCELLED (C) label to the heading. You can also cycle through these states (j/k).

- ~SPC SPC p {a,b,c,j,k,x}~: Assign priority A, B, or C to the heading. A is the highest while C is the lowest. You can also cycle through priorities (j/k), or remove them altogether (x).

- ~SPC SPC d {s,d,t,T}~: Assign a scheduled date (s) or deadline (d) to the heading. This will open the calendar and you can navigate through the dates using ~M-h/j/k/l~. You can also use today's date for both scheduling (t) and as a deadline (T).

- ~SPC SPC T~: Assign a tag to the headline. Note that the tag :triage: has a special meaning - it signals to *emacs-groundup* that you want to review the task in the future. Reviewing can be done using the 'triage' agenda view (~SPC o a t~).

* Agenda

They keybinding ~SPC o a~ opens agenda views which can help you plan your projects and your day-to-day actionables. Some useful views:

- ~SPC o a a~: Today's agenda.

- ~SPC o a p~: Triage and pruning. Periodic review is recommended.

