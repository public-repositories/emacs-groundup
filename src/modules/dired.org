
* dired-x

- A more advanced dired.

#+BEGIN_SRC emacs-lisp
(use-package dired-x
  :straight nil
  :hook (dired-mode . (lambda () (interactive) (dired-omit-mode)))
  :config
  (setq dired-dwim-target t
        find-file-visit-truename nil
        dired-omit-files "^\\.?#\\|^\\.$\\|^\\.\\.$\\|^\\..*$")

  (when (equal system-type 'darwin)
    ;; (setq insert-directory-program "/opt/homebrew/bin/gls"))
    (setq insert-directory-program +v/path-ls-darwin))

  (add-hook 'dired-mode-hook #'+h/disable-visual-line-wrapping-hook))
#+END_SRC


* dired-quick-sort

Because ain't nobody got the time to faff around with listing switches.

#+BEGIN_SRC emacs-lisp
(use-package dired-quick-sort
  :straight t
  :after dired
  :init
  (add-hook 'dired-mode-hook 'dired-quick-sort))
#+END_SRC


* dired-du

To query size of folders.

#+BEGIN_SRC emacs-lisp
(use-package dired-du
  :straight t
  :after dired
  :config
  (setq dired-du-size-format t))
#+END_SRC


* f: use ace-window to visit files in dired

- Needs `ace-window`. If ace-window is not found, does nothing.

- Bound to <tab>

#+BEGIN_SRC emacs-lisp
(defun +f/aw-visit-file-from-dired ()
  "Use ace window to select a window for opening a file from dired."
  (interactive)

  (when (require 'ace-window nil 'noerror)
    (require 'ace-window)
    (let ((file (dired-get-file-for-visit)))
      (if (> (length (aw-window-list)) 1)
          (aw-select "" (lambda (window)
                          (aw-switch-to-window window)
                          (find-file file)))
        (find-file-other-window file)))))
#+END_SRC


* f: ask whether to kill dired buffer before quitting

#+BEGIN_SRC emacs-lisp
(defun +f/prompt-then-kill-current-dired-buffer ()
  "Prompt and kill dired buffer."
  (interactive)

  (if (y-or-n-p "kill this dired buffer?")
      (kill-current-buffer)))
#+END_SRC


* f: project aware dired jumps

#+BEGIN_SRC emacs-lisp
(defun +f/dired-jump ()
  "Project-aware dired-jump."
  (interactive)

  (if (project-current)
      (call-interactively 'project-dired)
    (call-interactively 'dired-jump)))
#+END_SRC


* keybindings

#+BEGIN_SRC emacs-lisp
(general-define-key
 :states '(visual normal motion)
 :keymaps 'dired-mode-map
 "SPC"  nil
 "TAB"  #'+f/aw-visit-file-from-dired
 "o"    'dired-omit-mode
 "s"    #'hydra-dired-quick-sort/body
 "q"    #'+f/prompt-then-kill-current-dired-buffer)

(+m/map-leader
 ;; "d" '(nil :wk "dired")

 "d d" '(dired-jump :wk "jump here")
 "d o" '(dired-jump-other-window :wk "jump here ow")

 "d D" '(+f/dired-jump :wk "jump")
 "d O" `(,(+m/ow +f/dired-jump) :wk "jump ow"))
#+END_SRC
