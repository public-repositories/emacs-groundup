
* vertico

Minibuffer completion.

#+BEGIN_SRC emacs-lisp
(use-package vertico
  :straight t
  :init (vertico-mode)
  :config
  (setq vertico-cycle t)

  ;; if jinx is installed integrate it with vertico 
  (when (require 'jinx nil 'noerror)
    (require 'jinx)
    (add-to-list 'vertico-multiform-categories
                 '(jinx grid (vertico-grid-annotate . 20)))
    (vertico-multiform-mode 1)))
#+END_SRC


* orderless

Ordering and fuzzy searching through minibuffer suggestions.

#+BEGIN_SRC emacs-lisp
(use-package orderless
  :straight t
  :init
  (setq completion-styles '(orderless basic)
        completion-category-defaults nil
        completion-category-overrides '((file (styles partial-completion)))))
#+END_SRC


* marginalia

Details in the minibuffer suggestions listings.

#+BEGIN_SRC emacs-lisp
(use-package marginalia
  :straight t
  :init (marginalia-mode 1))
#+END_SRC


* nerd-icons-completion

- This is a COUPLED package.
- It assumes that both nerd-icons and marginalia are installed.
- REF: https://github.com/rainstormstudio/nerd-icons-completion

#+BEGIN_SRC emacs-lisp
(use-package nerd-icons-completion
  :straight t
  :after marginalia
  :hook (marginalia-mode . nerd-icons-completion-marginalia-setup)
  (nerd-icons-completion-mode))
#+END_SRC


* keybindings

#+BEGIN_SRC emacs-lisp
(general-define-key
 :mode evil-insert-state-minor-mode
 :states 'insert
 "C-k" nil)

(general-define-key
 :keymaps 'vertico-map
 "C-j" 'vertico-next
 "C-k" 'vertico-previous)
#+END_SRC
