
* which-key

Which key possible keyboard completions.

#+BEGIN_SRC emacs-lisp
(use-package which-key
  :straight t
  :config
  (which-key-mode 1))
#+END_SRC


* evil

** evil

#+BEGIN_SRC emacs-lisp
(use-package evil
  :straight t
  :init
  (setq evil-want-integration t
	    evil-want-keybinding nil
        evil-undo-system 'undo-fu
        evil-respect-visual-line-mode t)

  :config
  (setq
   evil-auto-indent t
   evil-want-fine-undo t
   ;; evil-default-cursor t
   evil-want-minibuffer t
   evil-overriding-maps t)

  (evil-normalize-keymaps)
  (evil-mode 1))
#+END_SRC


** evil-collection

#+BEGIN_SRC emacs-lisp
(use-package evil-collection
  :straight t
  :after evil
  :custom (evil-collection-setup-minibuffer t)
  :init (evil-collection-init))
#+END_SRC


** evil-surround

#+BEGIN_SRC emacs-lisp
(use-package evil-surround
  :straight t
  :after evil
  :config (global-evil-surround-mode 1))
#+END_SRC


** evil-org

#+BEGIN_SRC emacs-lisp
(use-package evil-org
  :straight t
  :after (org evil)
  :hook (org-mode . (lambda () evil-org-mode))
  :config
  (require 'evil-org-agenda)
  (evil-org-agenda-set-keys))
#+END_SRC


** evil-escape

#+BEGIN_SRC emacs-lisp
(use-package evil-escape
  :straight t)
#+END_SRC


* key-chord

- Escape insert state with: {"jk","jj","kk","kj"}

- Only used for escaping from insert state

- Seems to be working slowly. Needs time to reset before it works. Don't know why.

#+BEGIN_SRC emacs-lisp
(use-package key-chord
  :straight t
  ;; :after evil
  :config
  (key-chord-define evil-insert-state-map "jk" 'evil-normal-state)
  (key-chord-define evil-insert-state-map "kj" 'evil-normal-state)
  (key-chord-define evil-insert-state-map "jj" 'evil-normal-state)
  (key-chord-define evil-insert-state-map "kk" 'evil-normal-state)

  (key-chord-mode 1))
#+END_SRC


* general

** general

#+BEGIN_SRC emacs-lisp
(use-package general
  :straight t
  :after evil
  :config
  (general-evil-setup t)
  (general-override-mode 1)
  (general-auto-unbind-keys))
#+END_SRC


** dealing with ~*Messages*~ in evil

The ~*Messages*~ buffer only recognises leader key after changing modes.

Ref1: https://github.com/noctuid/general.el/issues/493
Ref2: https://github.com/noctuid/evil-guide#why-dont-keys-defined-with-evil-define-key-work-immediately

#+BEGIN_SRC emacs-lisp
(with-eval-after-load 'evil
  (general-add-hook
   'after-init-hook
   (lambda (&rest _)
     (when-let ((messages-buffer (get-buffer "*Messages*")))
       (with-current-buffer messages-buffer
         (evil-normalize-keymaps))))
   nil nil t))
#+END_SRC



* hydra

#+BEGIN_SRC emacs-lisp
(use-package hydra
  :straight t)
#+END_SRC


* keybindings

** general

- Using override map for defining leader key ensures dired, magit etc. don't block it.

#+BEGIN_SRC emacs-lisp
;; leader
(general-create-definer +m/map-leader
  :states '(visual normal motion)
  :keymaps 'override
  :prefix +v/leader
  "" '(:ignore t :wk "leader"))

;; modeleader
(general-create-definer +m/map-modeleader
  :states '(visual normal motion)
  :prefix (concat +v/leader " " +v/modeleader)
  "" '(:ignore t :wk "modeleader"))
#+END_SRC


** leader map sub-menus

- This introduces coupling. However, the bindings are an integral part of emacs-groundup and should be considered a part of the distro.


#+BEGIN_SRC emacs-lisp
(+m/map-leader

  ;; symbols
  "," '(nil :wk "bookmarks")
  "." '(nil :wk "registers")
  "/" '(nil :wk "utils")
  ";"    '(nil :wk "comment")

  ;; a-z
  "b" '(nil :wk "buffers")
  ;; c -> capture
  "d" '(nil :wk "dired")
  "e" '(nil :wk "eval")
  "f" '(nil :wk "files")
  "h" '(nil :wk "help")
  "l" '(nil :wk "loops")
  "m" '(nil :wk "magit")
  "n" '(nil :wk "narrow/widen")
  "o" '(nil :wk "open")
  "p" '(nil :wk "project")
  "q" '(nil :wk "quit")
  "s" '(nil :wk "search")
  "t" '(nil :wk "terminal")
  "w" '(nil :wk "window")
  "<tab>" '(nil :wk "tabspaces"))
#+END_SRC


** scroll other window

#+BEGIN_SRC emacs-lisp
;; Scroll non-focussed window
(general-define-key
 :keymaps 'override
 "C-M-k" 'scroll-other-window
 "C-M-j" 'scroll-other-window-down)
#+END_SRC


