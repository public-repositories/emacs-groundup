
* py-isort

#+BEGIN_SRC emacs-lisp
(use-package py-isort
  :straight t
  :after python)
#+END_SRC


* python-black

#+BEGIN_SRC emacs-lisp
(use-package python-black
  :straight t
  :demand t
  :after python)
#+END_SRC


* hook: prettify-symbols

#+BEGIN_SRC emacs-lisp
(add-hook
 'python-mode-hook
 (lambda () (+f/prettify-symbols-push-to-alist
             '(("def"    . ?𝒇) ;; ʩ ƒ ⨍ 𝒇
               ("class"  . ?𝓢)
               ("for"    . ?∀)
               ("in"     . ?∈)
               ("return" . ?⟼)
               ("True"   . ?𝕋)
               ("False"  . ?𝔽)
               ("int"    . ?ℤ)
               ("float"  . ?ℝ)
               ("bool"   . ?𝔹)
               ("str"    . ?𝕊)
               ("None"   . ?∅)))))
#+END_SRC


* keybindings

#+BEGIN_SRC emacs-lisp
(+m/map-modeleader
 :keymaps 'python-mode-map

 "i" '(nil :wk "isort")
 "i i" '(py-isort-buffer :wk "buffer")
 "i I" '(py-isort-region :wk "region")
  
 "b" '(nil :wk "black")
 "b b" '(python-black-buffer :wk "buffer")
 "b B" '(python-black-region :wk "region")

  "r" '(nil :wk "REPL")
  "r t" '(run-python :wk "open")
  "r r" '(python-shell-send-region :wk "eval region")
  "r b" '(python-shell-send-buffer :wk "eval buffer"))
#+END_SRC

