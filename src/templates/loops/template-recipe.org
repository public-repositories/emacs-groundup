#+TITLE: ${title}
#+CATEGORY: personal
#+FILETAGS: :recipe:%^G

* Ingredient list

|------------+------|
| Ingredient | Qty. |
|------------+------|
| ...        | ...  |
|------------+------|

* Recipe

1. %?
